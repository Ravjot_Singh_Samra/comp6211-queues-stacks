﻿using System;
using System.Collections;

namespace comp6211_queues
{
    class Program
    {
        static void Main(string[] args)
        {
            switch(int.Parse(Console.ReadLine()))
            {
                case 1:
                    queuesTest();
                    break;
                case 2:
                    stacksTest0();
                    break;
                case 3:
                    stacksTest1();
                    break;
            }
        }

        public static void queuesTest()
        {
            Queue myQ = new Queue();

            myQ.Enqueue("Test");
            myQ.Enqueue("Secondtest");

            foreach(string c in myQ)
            {
                Console.Write(c + ", ");
            }

            Console.Write("\b\u007f\b\u007f\n");

            Console.WriteLine("Removed: {0}", (string)myQ.Dequeue());
        }

        public static void stacksTest0()
        {
            Stack myStk = new Stack();

            Console.WriteLine("Enter what you wish to be reversed:");

            char[] userInput = Console.ReadLine().ToCharArray();

            for(int i = 0; i < userInput.Length; i++)
            {
                myStk.Push(userInput[i]);
            }

            foreach(char s in myStk)
            {
                Console.Write(s);
            }
        }

        public static void stacksTest1()
        {
            Stack myStk = new Stack();

            Console.WriteLine("Enter a formula or programming statement:");

            char[] userInput = Console.ReadLine().ToCharArray();
            int balance = 0;
            
            for(int i = 0; i < userInput.Length; i++)
            {
                switch(userInput[i])
                {
                    case '(':
                        balance++;
                        break;
                    case ')':
                        balance++;
                        break;
                    case '[':
                        balance++;
                        break;
                    case ']':
                        balance++;
                        break;
                    case '{':
                        balance++;
                        break;
                    case '}':
                        balance++;
                        break;
                    case '<':
                        balance++;
                        break;
                    case '>':
                        balance++;
                        break;
                }
                
                myStk.Push(userInput[i]);
            }

            if(balance % 2 == 0)
            {
                Console.WriteLine("BALANCE OK: {0}", balance);
            }
            else
            {
                int balanceRemain = balance % 2;
                Console.WriteLine("BALANCE INCORRECT: {0}, {1} STRAY BRACKET(S)", balance, balanceRemain);
            }
        }
    }
}
